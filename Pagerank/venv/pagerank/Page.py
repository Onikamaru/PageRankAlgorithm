from pagerank.Link import Link

class Page:

    def __init__(self ,pagerank ,name ):
        self.pagerank = pagerank
        self.name = name
        self.ausgehendeLinks = []
        self.eingehendeLinks = []

    def addAusgehenderLink(self, page):
        link = Link(self, page)
        self.ausgehendeLinks.append(link)

    def addEingehenderLink(self, page):
        link = Link(page, self)
        self.eingehendeLinks.append(link)