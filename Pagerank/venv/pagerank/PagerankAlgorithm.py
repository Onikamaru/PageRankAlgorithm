class PagerankAlgorithm:

    def __init__(self, listeAllerSeiten, d):
        self.listeAllerSeiten = listeAllerSeiten
        self.d = d

    def calc(self):
        for page in self.listeAllerSeiten:
            ergebnissSummenZeichen = 0
            for link in page.eingehendeLinks:
                ergebnissSummenZeichen = ergebnissSummenZeichen + link.pageA.pagerank / len(link.pageA.ausgehendeLinks)
            page.pagerank = 1-self.d + self.d * ergebnissSummenZeichen

    def printPages(self):
        self.bubbleSort(self.listeAllerSeiten)
        for page in self.listeAllerSeiten:
            print(page.name + " " + str(page.pagerank))

    def bubbleSort(self, pages):
        n = len(pages)

        # Traverse through all array elements
        for i in range(n - 1):
            # range(n) also work but outer loop will repeat one time more than needed.

            # Last i elements are already in place
            for j in range(0, n - i - 1):

                # traverse the array from 0 to n-i-1
                # Swap if the element found is greater
                # than the next element
                if pages[j].pagerank > pages[j + 1].pagerank:
                    pages[j], pages[j + 1] = pages[j + 1], pages[j]