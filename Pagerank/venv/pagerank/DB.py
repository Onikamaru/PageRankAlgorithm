import mysql.connector

class DB:
    def __init__(self):
        self.con = mysql.connector.connect(user='root', password='example',host='localhost',database='wiki');

    def selectPageLinks(self):
        mycursor = self.con.cursor()
        mycursor.execute("SELECT page_title von , pl_title nach From page join pagelinks on page_id = pl_from")
        myresult = mycursor.fetchall()
        pages = []
        for page_title, pl_title in myresult:
            pages.append((page_title,pl_title))
        return pages

    def selectDistinctPages(self):
        mycursor = self.con.cursor()
        mycursor.execute("SELECT page_title,page_id From page")
        myresult = mycursor.fetchall()
        pages = []
        for page_title,page_id in myresult:
            pages.append(page_title)
        return pages

    def closeConnection(self):
        self.con.close();
