from pagerank.Page import Page
from pagerank.PagerankAlgorithm import PagerankAlgorithm
from pagerank.DB import DB

def linkTwoPages(pageOne, pageTwo):
    pageOne.addAusgehenderLink(pageTwo)
    pageTwo.addEingehenderLink(pageOne)

def createPages(pagesString):
    pages = []
    for pageString in pagesString:
        pages.append(Page(1,pageString))
    return pages

def linkDatabasePages(pagesLinkString,pages):
    for linkString in pagesLinkString:
        pageA = None
        pageB = None
        for page in pages:
            if(page.name==linkString[0]):
                pageA=page
            if(page.name==linkString[1]):
                pageB=page
            if(pageA != None and pageB != None):
                linkTwoPages(pageA, pageB)
                continue



database = DB()
pagesString = database.selectDistinctPages()
pagesLinkString = database.selectPageLinks()
database.closeConnection()
pages = createPages(pagesString)
linkDatabasePages(pagesLinkString,pages)

algorithm = PagerankAlgorithm(pages,0.85)
for i in range (0, 5):
    algorithm.calc()
algorithm.printPages()



